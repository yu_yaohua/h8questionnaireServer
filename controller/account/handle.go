package account

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"h8QuestionnaireServer/pojo/request"
	"h8QuestionnaireServer/pojo/response"
	"h8QuestionnaireServer/service"
	"net/http"
)

func saveAccountDetailInfo(ctx *gin.Context) {
	var param request.WxAccountInfo
	err := ctx.BindQuery(&param)
	openid, _ := ctx.Get("openid")
	if err != nil {
		ctx.JSON(http.StatusOK, response.GenError(response.PARAM_ERROR, ""))
	} else {
		log.Info().Msgf("%s", param)
	}

	service.SaveAccount(openid.(string), param)
	ctx.JSON(200, response.GenSuccess("保存信息成功"))
}

func findAccountById(ctx *gin.Context) {
	id := ctx.Query("id")
	user, _ := service.FindAccountById(id)
	ctx.JSON(200, user)
}

func saveAccount(ctx *gin.Context){
	openid := ctx.Query("openid")
	unionid := ctx.Query("unionid")
	service.SaveAccountInfo(openid, unionid)
	ctx.JSON(200, "Success")
}

package account

import (
	"github.com/gin-gonic/gin"
	"h8QuestionnaireServer/middleware"
)

func LoadAccount(e *gin.Engine) {
	// 用户管理接口
	accountGrp := e.Group("/account")
	{
		accountGrp.Use(middleware.TokenAuth())
		accountGrp.GET("/saveAccountDetailInfo", saveAccountDetailInfo)
		accountGrp.GET("/findAccountById", findAccountById)
		accountGrp.GET("/saveAccount", saveAccount)
	}
}

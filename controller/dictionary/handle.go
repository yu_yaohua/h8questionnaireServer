package dictionary
import (
	"github.com/gin-gonic/gin"
	"h8QuestionnaireServer/pojo/response"
	"h8QuestionnaireServer/service"
	"net/http"
)

func SelectProvince(ctx *gin.Context){

	dicts,err := service.GetProvinceServer()
	if err == nil{
		resp := response.GenSuccess(dicts)
		ctx.JSON(http.StatusOK,resp)
	}else{
		resp := response.GenLoser(err)
		ctx.JSON(http.StatusOK,resp)
	}
}
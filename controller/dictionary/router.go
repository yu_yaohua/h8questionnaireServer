package dictionary
import "github.com/gin-gonic/gin"

func LoadProvince(e *gin.Engine){
	dicts := e.Group("/dicts")
	dicts.GET("/SelectProvince",SelectProvince)
}
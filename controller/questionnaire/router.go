package questionnaire

import "github.com/gin-gonic/gin"

func LoadQuestionnaire(e *gin.Engine){
	questionnaire := e.Group("/questionnaire")
	questionnaire.POST("/InsertQuestionnaire",InsertQuestionnaire)
	questionnaire.GET("/SelectQuestionnaireData",SelectQuestionnaireData)
	questionnaire.POST("/SelectQuestionnaireData",SelectQuestionnaireData)
	questionnaire.POST("/Login",UserLogin)
}

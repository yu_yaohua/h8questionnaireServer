package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"h8QuestionnaireServer/controller/account"
	"h8QuestionnaireServer/controller/dictionary"
	"h8QuestionnaireServer/controller/profile"
	"h8QuestionnaireServer/controller/questionnaire"
)

func Init(router *gin.Engine) {
	log.Info().Msg("init controller")
	// 微信小程序登录接口
	profile.LoadProfile(router)
	// 用户信息接口
	account.LoadAccount(router)
	//问卷调查接口
	questionnaire.LoadQuestionnaire(router)
	//字典接口
	dictionary.LoadProvince(router)
}

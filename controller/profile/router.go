package profile

import (
	"github.com/gin-gonic/gin"
)

func LoadProfile(e *gin.Engine) {
	userGrp := e.Group("/profile")
	{
		userGrp.GET("/auth", AuthProfile)
	}
}

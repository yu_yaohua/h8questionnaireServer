package main

import (
	"github.com/gin-gonic/gin"
	"h8QuestionnaireServer/components/config"
	"h8QuestionnaireServer/controller"
	_ "h8QuestionnaireServer/database"
	"h8QuestionnaireServer/middleware"
)

var router *gin.Engine

// 市八调查问卷服务
type Student struct {
	QuestionnaireId      string `json:"QuestionnaireId"`
	Version              int `json:"Version"`
	Content              string `json:"Content"`
}



func main() {

	//sid:=service.GetUnicID()
	//if sid == ""{
	//
	//}
	// 默认使用gin.Recovery()与gin.Logger()中间件
	router = gin.Default()

	router.Use(middleware.Cors())

	controller.Init(router)
	router.Run(":" + config.Config.Port)

	//byte,err := qrcode.Encode("123456",qrcode.Medium,256)
	
}

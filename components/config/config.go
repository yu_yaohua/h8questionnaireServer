package config

import (
	"log"

	"github.com/jinzhu/configor"
)

var Config = struct {
	Port string `default:"port"`
	DB   struct {
		Ip       string
		Port     string
		Schema   string
		Name     string
		Password string
	}
	Cache struct {
		Ip   string
		Port string
	}
	Wxapp struct {
		Appid       string
		Secret      string
	}
	Limit_Hour string `default:"limit_hour"`
}{}

func init() {
	err := configor.Load(&Config, "conf/application.yml")
	if err != nil {
		panic(err)
	}
	log.Printf("config %v\n", Config)
}

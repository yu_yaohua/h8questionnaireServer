package service

import (
	"h8QuestionnaireServer/components/config"
	"h8QuestionnaireServer/database/dao"
	"h8QuestionnaireServer/database/entity"
	"time"
)

func InsertQuestionDataServer(content string, version string) (result int, id string, err error) {

	result, id, err = dao.InsertQuestionnaireData(Snowflake.GenId(), content, version)
	return
}

func GetQuestionDataServer(token string,code string) (dicts []*entity.DictQuestionnaireSelect, err error) {

	empdict := new(entity.DictEmployee)
	dicts, empdict, err = dao.SelectAllQuestionnaire(code)
	if err != nil {
		return nil, err
	}

	//h, _ := time.ParseDuration(config.Config.Limit_Hour)
	//time.ParseInLocation()
	emp_token := IsExpired(empdict)
	if empdict != nil && (emp_token != token) {
		dicts = nil
		return
	}
	empdict.LimitTime = time.Now()
	empdict.Token = ""
	empdict.Name = ""
	empdict.Pwd = ""

	err = dao.FreshToken(empdict)
	return
}
//判断是否过期
func IsExpired(dict *entity.DictEmployee)(emp_token string){
	h, _ := time.ParseDuration(config.Config.Limit_Hour)
	var result bool = dict.LimitTime.Add(h).After(time.Now())
	if result {
		emp_token = dict.Token;
	} else {
		emp_token = ""
	}
	return emp_token
}

func GetUnicID() (uid string) {

	uid = Snowflake.GenId()
	return
}

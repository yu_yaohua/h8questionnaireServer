package service

import (
	"h8QuestionnaireServer/database/dao"
	"h8QuestionnaireServer/database/entity"
)

func GetProvinceServer() (dicts []*entity.DictProvince, err error) {
	dicts, err = dao.SelectAllProvince()
	return
}

func CheckLogin(code, pwd string) (ret bool, err error) {
	emp, err := dao.GetLoginInfo(code)
	if err != nil || emp == nil || emp.Code == "" {
		ret = false
		return
	}
	if emp.Pwd == pwd {
		ret = true
		return
	}
	return
}
func SaveToken(dict * entity.DictEmployee) error {

	err := dao.FreshToken(dict)
	return err
}
//func SaveToken(code, name, token string, limit time.Time) error {
//
//	err := dao.FreshToken(code, token, limit)
//	return err
//}

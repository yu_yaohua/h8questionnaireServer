package service

import (
	"h8QuestionnaireServer/database/dao"
	. "h8QuestionnaireServer/database/entity"
)

func FindAccountById(id string) (user *AccountInfo, err error) {
	return dao.FindUserById(id)
}

func FindAccountByOpenid(openid string) (user *AccountInfo, err error) {
	return dao.FindUserByOpenId(openid)
}
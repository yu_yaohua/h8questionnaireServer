package dao

import (
	"github.com/rs/zerolog/log"
	"h8QuestionnaireServer/database"
	"h8QuestionnaireServer/database/entity"
)

func InsertAccount(openid, unionid string) (accountInfo *entity.AccountInfo, err error) {
	user := entity.AccountInfo{
		Openid:  openid,
		Unionid: unionid,
	}

	//database.GetPostgreDB().Debug().Joins().Create(&user)
	return &user, nil
}

func FindUserByOpenId(openid string) (user *entity.AccountInfo, err error) {
	user = new(entity.AccountInfo)
	log.Info().Msgf("准备查询数据，openid=%s", openid)
	err = database.GetPostgreDB().Debug().Where("openid = ?", openid).First(&user).Error
	log.Info().Msgf("查询到的数据=%v", user)
	if err != nil {
		log.Error().Msgf("查询错误=%v", err)
	}
	return
}

func FindUserById(id string) (user *entity.AccountInfo, err error) {
	user = new(entity.AccountInfo)
	err = database.GetPostgreDB().Where("id = ?", id).First(&user).Error
	return
}

package dao

import (
	"github.com/rs/zerolog/log"
	"h8QuestionnaireServer/database"
	"h8QuestionnaireServer/database/entity"
)

func SelectAllProvince()(dicts []*entity.DictProvince, err error){
	err = database.GetPostgreDB().Where("parent_id = ?","100000").Find(&dicts).Error
	if err != nil {
		log.Info().Msg("无数据！")
	}
	return
}

func GetLoginInfo(code string)(emp *entity.DictEmployee,err error){
	//createEmpTable()
	dict := new(entity.DictEmployee)
	err = database.GetPostgreDB().Where("code = ?",code).Find(&dict).Error
	if err != nil{
		log.Info().Msg("无数据")
	}else {
		emp = dict
	}
	return
}

func createEmpTable(){
	err := database.GetPostgreDB().AutoMigrate(&entity.DictEmployee{}).Error
	log.Printf("registering result %v.", err)
}


func FreshToken(dict * entity.DictEmployee) error{

	err := database.GetPostgreDB().Model(&dict).Where("code = ?",dict.Code).Update(dict).Error
	return err
}
//func FreshToken(code,token string,limit time.Time) error{
//
//
//	return nil
//}
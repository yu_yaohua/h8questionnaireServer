package entity

import "time"

type DictProvince struct {
	Code string
	Parent_id string
	Name string
}
func (DictProvince) TableName() string {
	return "dict.dict_Info"
}

type DictEmployee struct {
	Code string
	Name string
	Pwd string
	Token string
	LimitTime time.Time
}

func (DictEmployee) TableName() string {
	return "dict.dict_employee"
}
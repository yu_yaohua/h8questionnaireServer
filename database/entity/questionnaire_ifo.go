
package entity

import (
	"time"
)

type DictQuestionnaire struct {
	QuestionnaireId string `json:"questionnaire_id"`
	Version string `json:"version"`
	Content string `json:"content"`
	QuestionDate *time.Time `json:"question_date"`
}
func (DictQuestionnaire) TableName() string {
	return "business.questionnaire_data"
}

type DictQuestionnaireSelect struct {
	QuestionnaireId string `json:"questionnaire_id"`
	Version string `json:"version"`
	Content string `json:"content"`
	QuestionDate *time.Time `json:"question_date"`
	Id int `json:"id"`
}
func (DictQuestionnaireSelect) TableName() string {
	return "business.questionnaire_data"
}

type DictQuestionnaireAll struct {
	QuestionnaireId string
	Version string
	Content string
	QuestionDate *time.Time
	Id int
}

func (DictQuestionnaireAll) TableName() string {
	return "business.questionnaire_data"
}

type Response struct {
	Tips string
	Id string
}

package entity

import (
	"h8QuestionnaireServer/database"
)

type AccountInfo struct {
	database.Model
	Openid    string
	Unionid   string
	Name      string
	Gender    string
	Type      string
	Level     string
	InviterId string `gorm:"column:inviter_id"`
	InviteWay string `gorm:"column:invite_way"`
	AvatarUrl string `gorm:"column:avatar_url"`
}

func (AccountInfo) TableName() string {
	return "health_assistant.account_info"
}


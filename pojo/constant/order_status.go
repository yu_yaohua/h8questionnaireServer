package constant

const (
	OrderStatus_Created    = "created"
	OrderStatus_PendingPay = "pending_pay"
	OrderStatus_Paid       = "paid"
	OrderStatus_Cancelled  = "cancelled"
)

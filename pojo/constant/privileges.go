package constant

const (
	Privilege_User           = "user"
	Privilege_Chinese_Doctor = "chinese_doctor"
	Privilege_Assistant      = "assistant"
)

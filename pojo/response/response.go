package response

type Response struct {
	Code   string `json:"code"`
	Info   string `json:"info"`
	Result interface{} `json:"result,omitempty"`
}

type ResponseCode struct {
	Code string
	Info string
}

var SUCCESS = &ResponseCode{Code: "RC00000", Info: "请求成功"}
var GENERAL_ERROR = &ResponseCode{Code: "RC60000", Info: "出错提示"}
var PARAM_ERROR = &ResponseCode{Code: "RC80000", Info: "参数错误"}
var SYS_ERROR = &ResponseCode{Code: "RC90000", Info: "系统错误"}
var SYS_LOSER = &ResponseCode{Code: "RC99999", Info: "请求失败"}
var SYS_SUCCESSWARN = &ResponseCode{Code: "RC00001", Info: "请求成功，数据为空！"}

func GenSuccessWarn(result interface{}) (resp *Response) {
resp = Gen(SYS_SUCCESSWARN, "", result)
return
}

func GenLoser(result interface{}) (resp *Response) {
	resp = Gen(SYS_LOSER, "", result)
	return
}
func GenSuccessPost(info string,result interface{}) (resp *Response) {
	resp = Gen(SUCCESS, info, result)
	return
}

func GenSuccess(result interface{}) (resp *Response) {
	resp = Gen(SUCCESS, "", result)
	return
}

func GenError(code *ResponseCode, info string) (resp *Response) {
	resp = Gen(code, info, nil)
	return
}

func Gen(code *ResponseCode, info string, result interface{}) (resp *Response) {
	resp = &Response{Code: code.Code}
	if len(info) > 0 {
		resp.Info = info
	} else {
		resp.Info = code.Info
	}
	resp.Result = result
	return
}

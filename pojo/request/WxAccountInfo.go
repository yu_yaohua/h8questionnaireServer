package request

type WxAccountInfo struct {
	Nickname  string `form:"nickname"`
	Gender    string `form:"gender"`
	AvatarUrl string `form:"avatarUrl"`
}
